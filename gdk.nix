 {config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
      ruby
      postgresql
      redis
      runit
      gcc
      rake
      yarn
      nodejs-13_x
      go
      graphicsmagick
      exiftool
      minio
      sqlite
    ];

  services.postgresql.enable = true;
}
